package model;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name="flight")
public class Flight {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private int flightnumber;
    private String type;
    private String departure;
    private Timestamp departuretime;
    private String arrival;
    private Timestamp arrivaltime;

    public Flight() {
    }

    @Override
    public String toString() {
        return "Flight{" +
                "id=" + id +
                ", flightnumber=" + flightnumber +
                ", type='" + type + '\'' +
                ", departure='" + departure + '\'' +
                ", departuretime=" + departuretime +
                ", arrival='" + arrival + '\'' +
                ", arrivaltime=" + arrivaltime +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFlightnumber() {
        return flightnumber;
    }

    public void setFlightnumber(int flightnumber) {
        this.flightnumber = flightnumber;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDeparture() {
        return departure;
    }

    public void setDeparture(String departure) {
        this.departure = departure;
    }

    public Timestamp getDeparturetime() {
        return departuretime;
    }

    public void setDeparturetime(Timestamp departuretime) {
        this.departuretime = departuretime;
    }

    public String getArrival() {
        return arrival;
    }

    public void setArrival(String arrival) {
        this.arrival = arrival;
    }

    public Timestamp getArrivaltime() {
        return arrivaltime;
    }

    public void setArrivaltime(Timestamp arrivaltime) {
        this.arrivaltime = arrivaltime;
    }
}
