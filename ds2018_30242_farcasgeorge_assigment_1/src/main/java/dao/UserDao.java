package dao;

import model.Flight;
import model.User;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class UserDao {

    private SessionFactory sessionFactory;

    public UserDao() {
        Configuration configuration = new Configuration().configure("hibernate.cfg.xml");
        configuration.addAnnotatedClass(User.class);

        configuration.addAnnotatedClass(Flight.class);
        ServiceRegistry serviceRegistry
                = new StandardServiceRegistryBuilder()
                .applySettings(configuration.getProperties()).build();
         sessionFactory = configuration.buildSessionFactory(serviceRegistry);
    }

    public User getByUsername(String username){
        Session session=sessionFactory.openSession();
        Query createQuery =session.createQuery("from User Where username= :username");
        createQuery.setParameter("username",username);
        User user = (User) createQuery.uniqueResult();
        return user;
    }


}
