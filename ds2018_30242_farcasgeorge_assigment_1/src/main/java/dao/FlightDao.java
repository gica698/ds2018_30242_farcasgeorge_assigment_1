package dao;

import model.Flight;
import model.User;
import org.hibernate.*;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import java.util.List;

public class FlightDao {
    private SessionFactory sessionFactory;

    public FlightDao() {
        Configuration configuration = new Configuration().configure("hibernate.cfg.xml");
        configuration.addAnnotatedClass(User.class);

        configuration.addAnnotatedClass(Flight.class);
        ServiceRegistry serviceRegistry
                = new StandardServiceRegistryBuilder()
                .applySettings(configuration.getProperties()).build();
        sessionFactory = configuration.buildSessionFactory(serviceRegistry);
    }

    public List<Flight> findAll() {

        Session session = sessionFactory.openSession();
        return session.createCriteria(Flight.class).list();


    }

    public int createFlight(Flight flight){
        Session session = sessionFactory.openSession();
        return (int) session.save(flight);
    }

    public void deleteFlight(int id){
        Session session=sessionFactory.openSession();
        Query deltequery=session.createQuery("delete from Flight where id= :id");
        deltequery.setParameter("id",id);
        deltequery.executeUpdate();
    }

    public void updateFlight(Flight flight){
        Transaction tx=null;
        try {
            Session session = sessionFactory.openSession();
            tx=session.beginTransaction();
            Query updateQuery = session.createQuery("Update Flight  SET flightnumber= :flightnumber, type= :type, " +
                    "departure= :departure, arrival= :arrival, departuretime= :departuretime, arrivaltime= :arrivaltime where id= :id");
            updateQuery.setParameter("id", flight.getId());
            updateQuery.setParameter("flightnumber", flight.getFlightnumber());
            updateQuery.setParameter("type", flight.getType());
            updateQuery.setParameter("departure", flight.getDeparture());
            updateQuery.setParameter("departuretime", flight.getDeparturetime());
            updateQuery.setParameter("arrivaltime", flight.getArrivaltime());
            updateQuery.setParameter("arrival", flight.getArrival());
            updateQuery.executeUpdate();
            tx.commit();
        }
        catch(HibernateException e){
            if(tx!=null) tx.rollback();
            e.printStackTrace();
        }
    }

    public Flight getById(int id) {

        Session session = sessionFactory.openSession();
        Query createQuery = session.createQuery("from Flight Where id= :id");
        createQuery.setParameter("id", id);
        Flight flight = (Flight) createQuery.uniqueResult();
        return flight;
    }
}
