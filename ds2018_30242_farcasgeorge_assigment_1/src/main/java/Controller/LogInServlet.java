package Controller;

import dao.UserDao;
import model.User;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/LogIn")
public class LogInServlet extends HttpServlet {

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        UserDao userDao = new UserDao();
        User user = userDao.getByUsername(request.getParameter("username"));

        if (user == null)
            return;
        else if (!user.getPassword().equals(request.getParameter("password")))
            return;
        else if (user.isIsadmin()) {
            response.sendRedirect("admin.html");
        } else {
            response.sendRedirect("/user");
        }
    }
}
