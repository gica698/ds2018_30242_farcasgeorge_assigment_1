package Controller;

import dao.FlightDao;
import model.Flight;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@WebServlet("/admin")
public class AdminServlet extends HttpServlet {

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        FlightDao flightDao=new FlightDao();
        Flight flight=new Flight();
        String action=request.getParameter("action");

        if("create".equals(action)) {
            int flightnumber = Integer.parseInt(request.getParameter("flightnumber"));
            flight.setFlightnumber(flightnumber);
            String arrival = request.getParameter("arrival");
            flight.setArrival(arrival);
            String departure = request.getParameter("departure");
            flight.setDeparture(departure);
            String type = request.getParameter("type");
            flight.setType(type);
            String departuretime = request.getParameter("departuretime");
            Timestamp departuret = Timestamp.valueOf(LocalDateTime.parse(departuretime));
            flight.setDeparturetime(departuret);
            String arrivaltime = request.getParameter("arrivaltime");
            Timestamp arrivalt = Timestamp.valueOf(LocalDateTime.parse(arrivaltime));
            flight.setArrivaltime(arrivalt);
            flightDao.createFlight(flight);
        }
        else if ("delete".equals(action)){
            int id=Integer.parseInt(request.getParameter("id"));
            flightDao.deleteFlight(id);
        }
        else if("update".equals(action)){
        Flight flightl=flightDao.getById(Integer.parseInt(request.getParameter("id")));

            int flightnumber = Integer.parseInt(request.getParameter("flightnumber"));
            flightl.setFlightnumber(flightnumber);
            String arrival = request.getParameter("arrival");
            flightl.setArrival(arrival);
            String departure = request.getParameter("departure");
            flightl.setDeparture(departure);
            String type = request.getParameter("type");
            flightl.setType(type);
            String departuretime = request.getParameter("departuretime");
            Timestamp departuret = Timestamp.valueOf(LocalDateTime.parse(departuretime));
            flightl.setDeparturetime(departuret);
            String arrivaltime = request.getParameter("arrivaltime");
            Timestamp arrivalt = Timestamp.valueOf(LocalDateTime.parse(arrivaltime));
            flightl.setArrivaltime(arrivalt);
            flightDao.updateFlight(flightl);
            response.sendRedirect("admin.html");
        }

    }
}
