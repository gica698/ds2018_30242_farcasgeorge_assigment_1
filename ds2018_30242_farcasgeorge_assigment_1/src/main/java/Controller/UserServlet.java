package Controller;

import dao.FlightDao;
import model.Flight;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet("/user")
public class UserServlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        FlightDao flightdao = new FlightDao();

        List<Flight> flights = flightdao.findAll();

        PrintWriter printWriter = response.getWriter();

        printWriter.println("<table>\n" +
                "<tr>\n" +
                "<th> id </th>\n" +
                "<th> flightnumber </th>\n" +
                "<th> type </th>\n" +
                "<th> arrivalcity </th>\n" +
                "<th> arrivaltime </th>\n" +
                "<th> departurecity </th>\n" +
                "<th> departuretime </th>\n" +
                "</tr>");

        for (Flight flight :flights) {
            printWriter.println("<tr>\n" +
                    "<th>"+flight.getId()+"</th>\n" +
                    "<th>"+flight.getFlightnumber()+"</th>\n" +
                    "<th>"+flight.getType()+"</th>\n" +
                    "<th>"+flight.getArrival()+"</th>\n" +
                    "<th>"+flight.getArrivaltime()+"</th>\n" +
                    "<th>"+flight.getDeparture()+"</th>\n" +
                    "<th>"+flight.getDeparturetime()+"</th>\n" +
                    "</tr>");
        }

        printWriter.println("</table>");

    }

}
